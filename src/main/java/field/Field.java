/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package field;

import game.Player;
import game.Settings;
import units.RemoteControl;
import java.util.List;

/**
 *
 * @author nick
 */
public interface Field {
    Radar getRadar();
    List<RemoteControl> getControlsForPlayer(Player player);
    void update();
    void initialize(Settings settings);
}
