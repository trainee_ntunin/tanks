/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package field;

import units.RadarsObject;
import java.util.List;

/**
 *
 * @author nick
 */
public interface Radar {
    List<RadarsObject> getObjects();
}
