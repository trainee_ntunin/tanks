package Rules;

import units.Instruction;

import java.util.List;

/**
 * Created by Светлана on 14.12.2015.
 */
public interface Rule {
    void apply(List<Instruction> instructions);
}
