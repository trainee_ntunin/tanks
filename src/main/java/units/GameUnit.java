/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package units;

import geometry.RadiusVector;
import geometry.Shape;

/**
 *
 * @author nick
 */
public class GameUnit{
    private Instruction instruction;
    public void setInstruction(Instruction instruction) {
        this.instruction = instruction;
    }
    public Instruction getInstruction() {
        return instruction;
    }
}
