/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package units;

import geometry.RadiusVector;
import geometry.Shape;

/**
 *
 * @author nick
 */
public interface RadarsObject {
    Shape getShape();
    int getSpeed();
    RadiusVector getDirection();
}
