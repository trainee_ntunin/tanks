/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package units;

/**
 *
 * @author nick
 */
public class RemoteControl {
    GameUnit tank;
    public void moveForward() {
        tank.setInstruction(null/*move forward*/);
    }
    public void moveBack() {
        tank.setInstruction(null/*move back*/);
    }
    public void rotate(double angle) {
        tank.setInstruction(null /*rotate*/);
    }
    public void fire() {
        tank.setInstruction(null/*fire*/);
    }
}
