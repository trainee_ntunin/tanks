/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package geometry;

/**
 *
 * @author nick
 */
public class Circle implements Shape{
    RadiusVector position;
    int radius;
    public Circle(RadiusVector position, int radius) {
        this.position = position;
        this.radius = radius;
    }
    @Override
    public RadiusVector getPosition(){
        return position;
    }
    public int getRadius(){
        return radius;
    }
}
