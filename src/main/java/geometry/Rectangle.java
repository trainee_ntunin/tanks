/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package geometry;

/**
 *
 * @author nick
 */
public class Rectangle implements Shape{
    RadiusVector topLeftCorner;
    RadiusVector bottomRightCorner;
    public Rectangle(int topX, int topY,int bottomX, int bottomY) {
        topLeftCorner = new RadiusVector(topX, topY);
        bottomRightCorner = new RadiusVector(bottomX, bottomY);
    }
    @Override
    public RadiusVector getPosition() {
        return new RadiusVector(
                topLeftCorner.getX() + getWidth()/2, 
                topLeftCorner.getY() + getHeight()/2);
    }
    public int getWidth() {
        return Math.abs(bottomRightCorner.getX() - topLeftCorner.getX());
    }
    public int getHeight() {
        return Math.abs(bottomRightCorner.getX() - topLeftCorner.getX());
    }
}
