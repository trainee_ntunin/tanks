/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package geometry;

/**
 *
 * @author nick
 */
public class RadiusVector {
    int x;
    int y;
    public RadiusVector(int x, int y) {
        this.x = x;
        this.y = y;
    }
    public RadiusVector() {
        x = 0;
        y = 0;
    }
    public void setX(int x) {
        this.x = x;
    }
    public void setY(int y) {
        this.y = y;
    }
    public int getX(){
        return x;
    }
    public int getY(){
        return y;
    }
    
}
