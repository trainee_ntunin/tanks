/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package game;

import field.Radar;
import units.RemoteControl;

import java.util.List;

/**
 *
 * @author nick
 */
public interface Player {
    void makeStep();
    void applyStrategy(List<RemoteControl> controls, Radar radar);
}
