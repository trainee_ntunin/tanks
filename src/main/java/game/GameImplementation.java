package game;

import Rules.Rule;
import field.Field;
import field.Radar;
import units.GameUnit;
import units.Instruction;
import units.RemoteControl;

import java.util.ArrayList;
import java.util.List;
/**
 * Created by Светлана on 14.12.2015.
 */
public class GameImplementation {
    Settings  gameSettings;
    Field field;
    List<Player> players;
    List<RemoteControl> remoteControls;
    List<Strategy> strategies;
    List<GameUnit> units;
    List<Rule> rules;
    public void play() {
        gameSettings.read();
        field.initialize(gameSettings);
        //initialize journal

        while(needNextStep()) {
            Radar radar = getRadar();
            for (Player player : players) {
                player.applyStrategy(getRemoteControlsForPlayer(player), radar);
            }
            List<Instruction> instructions = getInstructions();

            for (Rule rule : rules)
                rule.apply(instructions);

            //checkRemotes();
            // journal.write();
        }
    }
    List<Instruction> getInstructions() {
        List<Instruction> instructions = new ArrayList<>();
        for(GameUnit unit: units) {
            instructions.add(unit.getInstruction());
        }
        return  instructions;
    }
    List<RemoteControl> getRemoteControlsForPlayer(Player player) {
        return null;
    }
    Radar getRadar() {
        return null;
    }
    boolean needNextStep() {
        //has time and there is not winner
        return true;
    }
    boolean hasTime(){
        return true;
    }
}
